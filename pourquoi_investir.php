<?php include('include/connexion_bdd.php'); ?>


<!DOCTYPE html>
<html>

<?php include('include/head.php'); ?>

<body class="nopnom marge_index">
    <div class="container nopnom">
        <?php
        include('include/header.php');
        include('include/navigation.php');
        ?>

        <div class="row" style="height: 25%; display : flex; justify-content : space-around; margin-bottom : 3em; margin-top: 10em;">
            <div style="font-size : 2.7em; letter-spacing : 0.03em; color : #70ad47; text-align: center;">
                <span style="font-weight : 500;">Pourquoi investir dans un FCPR</span><br/>
            </div>
        </div>

        <div class="row nopnom image_1_pq_invest_super_container">
            <div class="image_1_pq_invest_container" >
            <img src="images/pqinvestFCPR.png" class="image_1_pq_invest"/>
            </div>

            <div class="nopnom text_1_pq_invest_container">
            <div class="gros_titre" style="margin : 1em 0;  ">
                Des intérets économiques
            </div>
            <div class="sous_titre">
                Aider les PME à se développer
            </div>
            <div class="paragraphe">
                Les fonds FCPR ont l’obligation d’être investis en titres d’entreprises non cotées en bourse à hauteur de 50 % minimum.
                Ces investissements non côtés font l’objet de prise de participations dans de jeunes sociétés dans le but de les accompagner dans leur croissance
                et leurs choix stratégiques.
            </div>
            <div class="sous_titre">
                Aider les entrepreneurs à innover
            </div>
            <div class="paragraphe">
                Les investissements dans les entreprises, majoritairement faits lors d’augmentation de capital,
                sont un apport financier pour ces PME qui ont, à ce stade de maturité, encore peu de revenus et de grands besoins d’investissement.
            </div>
            <div class="sous_titre">
                Contribuer à l’emploi en France
            </div>
            <div class="paragraphe">
                Ces besoins de financement sont principalement des besoins d’embauche pour des entreprises en croissance.
                En investissant dans un FCPR vous favorisez directement l’emploi en France.
            </div>
            <div class="sous_titre">
                Financer l’économie réelle
            </div>
            <div class="paragraphe">
                A l’inverse de bon nombre de placements spéculatif ou de capitalisation, celui finance directement l’économie réelle et les entreprises de demain.
                Votre argent permet au financement des PME.
            </div>
        </div>
        </div>

        <div class="row nopnom image_2_pq_invest_super_container">
            <div class="image_2_pq_invest_container">
            <img src="images/des_interets_financiers.png" style="height : 100%;" />
            </div>

            <div class="text_2_pq_invest">

            <div class="gros_titre" style="margin : 1em 0;">
                Des intérêts financiers
            </div>

            <div class="sous_titre">
                Performance
            </div>

            <div class="paragraphe">
                Qui dit capital risque, dit performance. En acceptant de prendre des risques sur un produit non liquide investi dans des sociétés en développement,
                le potentiel de performance est beaucoup plus important que sur des livrets d’épargne.
            </div>

            <div class="sous_titre">
                Diversification
            </div>

            <div class="paragraphe">
                Investir dans un FCPR peut être une bonne manière de diversifier son épargne. En effet, le capital risque, ou private equity en anglais,
                est directement lié à l’économie réelle dans une vision long terme et n’est pas impacté par les aléas du marché boursier.
            </div>

            <div class="sous_titre">
                Défiscalisation
            </div>

            <div class="paragraphe">
                En complément des exonérations fiscales mise en place par le gouvernement pour l’investissement dans certains fonds,
                les FCPR vous donne droit en contreparti d’un blocage des fonds pendant cinq ans,
                d’être exonéré d’impôt sur le produit des parts qui vous est versé à la fois pendant la période de conservation de 5 ans mais également a posteriori
            </div>

            <div class="sous_titre">
                Exonération de plus value
            </div>

            <div class="paragraphe">
                L’investissement au sein d’un FCPR permet également, sous conditions, de bénéficier d’une exonération de plus-values.<br/>
                Pour qu’il y ait exonération, la cession des valeurs mobilières doit intervenir après la période de conservation de 5 ans.
            </div>
            </div>
        </div>

        <div class="row" style="display : flex; justify-content : center; margin-top : 8em; margin-bottom : 3em;">
            <div style="display : flex; justify-content : space-between; width : 50%;">
                <div class="vignette vignette_accueil">
                    <div style="height: 55%;">
                        <img src="images/vign1.png  " alt="" style="height : 100%; width : 100%;"/>
                    </div>
                    <div style="height: 45%;">
                        <div style="text-align : center; margin-top : 1em; width: 80%; margin-left :auto;margin-right :auto; height : 65%;  border-bottom : 0.5px solid #70ad47">
                            Qu’est ce que la finance non cotée ?
                        </div>
                    </div>
                </div>
                <div class="vignette vignette_accueil">
                    <div style="height: 55%;">
                        <img src="images/vign2.jpg  " alt="" style="height : 100%; width : 100%;"/>
                    </div>
                    <div style="height: 45%;">
                        <div style="text-align : center; margin-top : 1em; width: 80%; margin-left :auto;margin-right :auto; height : 65%;  border-bottom : 0.5px solid #70ad47">
                            Pourquoi investir dans le non cotée ?
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="display : flex; justify-content : center; margin-bottom : 3em;">
            <div style="display : flex; justify-content : space-between; width : 50%;">
                <div class="vignette vignette_accueil">
                    <div style="height: 55%;">
                        <img src="images/vign3.jpg  " alt="" style="height : 100%; width : 100%;"/>
                    </div>
                    <div style="height: 45%;">
                        <div style="text-align : center; margin-top : 1em; width: 80%; margin-left :auto;margin-right :auto; height : 65%;  border-bottom : 0.5px solid #70ad47">
                            Découvrir le fonctionnement d’un FCPR
                        </div>
                    </div>
                </div>
                <div class="vignette vignette_accueil">
                    <div style="height: 55%;">
                        <img src="images/vign4.jpg  " alt="" style="height : 100%; width : 100%;"/>
                    </div>
                    <div style="height: 45%;">
                        <div style="text-align : center; margin-top : 1em; width: 80%; margin-left :auto;margin-right :auto; height : 65%;  border-bottom : 0.5px solid #70ad47">
                            Quels sont les risques d’investir dans un FCPR ?
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include('include/articles.php'); ?>












<?php include('include/footer.php'); ?>
</div> <!-- container -->


<?php include('include/javascript_menu.php'); ?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>

$( document ).ready(function() {
    $('.vignette').each(function(){
        $(this).hover(function(){
            $(this).css('box-shadow','0px 1px 10px black');
            $(this).css('cursor','pointer');
        });
        $(this).mouseleave(function(){
            $(this).css('box-shadow','0px 0px 0px black');

        });

    });

    $('.boutton').each(function(){
        $(this).hover(function(){
            $(this).css('background-color','#70ad47');
            $(this).css('color','white');
            $(this).css('cursor','pointer');

        });
        $(this).mouseleave(function(){
            $(this).css('background-color','inherit');
            $(this).css('color','#70ad47');
        });
    });

    $('.titre_actu').each(function(){
        $(this).hover(function(){
            $(this).css('color','#bb0b0b');
        });

        $(this).mouseleave(function(){
            $(this).css('color','black');
        });
    });
})
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
