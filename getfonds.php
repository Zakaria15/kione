<?php
include('include/connexion_bdd.php');
function formatdate($date){
    $date_exploded = explode("-",$date);

    return $date_exploded[2] . "/" . $date_exploded[1] . "/" . $date_exploded[0];
}
$criteres_geo = $_GET['geo'];
$criteres_sec = $_GET['sec'];
$criteres_stat = $_GET['stat'];
$criteres_gestion = $_GET['gestion'];
$montant_minimum = $_GET['prix'];
$sous_ligne = $_GET['sous_ligne'];
$criteres_geo = str_replace("\"", "", $criteres_geo);
$criteres_sec = str_replace("\"", "", $criteres_sec);
$criteres_stat = str_replace("\"", "", $criteres_stat);
$finir_req = true;
$AND_a_rajouter = false;
$now = date("Y-m-d",time());
$query = 'SELECT * FROM fonds WHERE';
if(strlen($criteres_geo)>1){
    if($AND_a_rajouter){
      $query = $query . ' AND';
    }
    $criteres_geo_exploded = explode(",", $criteres_geo);
    $first_criteria = true;
    foreach($criteres_geo_exploded as &$value){
      if(!$first_criteria){
        $query = $query . ' OR ';
      }
      else{
        $query = $query . ' (';
      }
      $first_criteria = false;
      $query = $query . 'geographie LIKE ' . '\'%' . $value . '%\'';
    }
    $query = $query . ')';
    $AND_a_rajouter = true;
}
if(strlen($criteres_sec)>1){
  if($AND_a_rajouter){
    $query = $query . ' AND';
  }
  $criteres_sec_exploded = explode(",", $criteres_sec);
  $first_criteria = true;
  foreach($criteres_sec_exploded as &$value){
    if(!$first_criteria){
      $query = $query . ' OR ';
    }
    else{
      $query = $query . ' (';
    }
    $first_criteria = false;
    $query = $query . 'secteur LIKE ' . '\'%' . $value . '%\'';
  }
  $query = $query . ')';
  $AND_a_rajouter = true;
}
if(strlen($criteres_stat)>1){

  if($criteres_stat == "Ouvert actuellement"){
      if($AND_a_rajouter){
        $query = $query . ' AND';
    }
      $query = $query . ' duree_min_de_placement=0';
      $AND_a_rajouter = true;
  }else if($criteres_stat == "Toujours ouvert"){
      if($AND_a_rajouter){
        $query = $query . ' AND';
      }
      $query = $query . ' duree_min_de_placement!=0';
      $AND_a_rajouter = true;
  }

}
if($sous_ligne == 'true'){
  if($AND_a_rajouter){
    $query = $query . ' AND';
  }
$query = $query . ' souscription_en_ligne = ' . $sous_ligne;
$AND_a_rajouter = true;
}
if($AND_a_rajouter){
  $query = $query . ' AND';
}
$query = $query . ' souscription_min <= ' . $montant_minimum .' AND fin_souscription>\'' . $now .'\'';

// echo $query;

$req = $bdd->prepare($query);
$req->execute(array());

while ($donnees = $req->fetch())
{
    $req2 = $bdd->prepare('SELECT * FROM gestionnaires WHERE id = ?');
    $req2->execute(array($donnees['id_gestion']));
    $donnees2 = $req2->fetch();
    if($criteres_gestion == "" || strstr($criteres_gestion, $donnees2["nom"])){
        echo "<div class=\"row wrapper article\">";
        echo "<div class=\"col-4 col-lg-2 nopnom\" style=\"width : 100%; height : 100%\" >";
        echo "<img style=\"max-width : 100%; max-height : 100%; width : auto; height : auto; display: block; position : absolute; top : 50%; left : 50%; transform : translate(-50%, -50%);\" src=\"images/logo_gestion/" . $donnees2['img'] . "\" alt\"\"/>";
        echo "</div>";
        echo "<div class=\"col-8 col-lg-10\" style=\"padding-left : 0; padding-right : 0;\">";
        echo "<div class=\"row centrer premiere_ligne_fond\">";
        echo "<div class=\"col-12 col-lg-7\" style=\"line-height : 1.5em;\">";
        echo "<span class=\"nom_article\">" . $donnees['nom']. "</span>";
        echo "<br/>";
        echo "<span class=\"gestion_article\">Géré par " . $donnees2['nom'] . "</span>";
        echo "</div>";
        echo "<div class=\"col-2\" style=\"margin-left : 15px; padding-left : 0px; padding-right : 0px;\">";
        echo "<a href=\"" . $donnees['decouvrir'] ."\" target=\"_blank\">";
        echo "<div class=\"bouton_article_dec\"> DECOUVRIR </div>";
        echo "</a>";
        echo "</div>";
        if($donnees['souscription_en_ligne']){
          echo "<div class=\"col-1\">";
          echo "</div>";
          echo "<div class=\"col-2\" style=\"margin-left : 15px; padding-left : 0px; padding-right : 0px;\">";
          echo "<a href=\"" . $donnees['souscrire'] ."\" target=\"_blank\">";
          echo "<div class=\"bouton_article_sous\"> SOUSCRIRE </div>";
          echo "</a>";
          echo "</div>";
        }
        else{
            echo "<div class=\"col-1\">";
            echo "</div>";
            echo "<div class=\"col-2\">";
            echo "</div>";
        }
        echo "</div>";
        echo "<div class=\"row centrer deuxieme_ligne_fond\">";
        echo "<div class=\"col-12 col-lg-3\">";
        if($donnees['duree_min_de_placement'] > 0){
            $statut = "Toujours ouvert";
        }else{
            $statut = "Ouvert actuellement";
        }
        echo "<span class=\"gras\">Statut : </span>" . $statut;
        echo "<br/>";
        echo "<span class=\"gras\">Souscription minimum : </span>" . number_format($donnees['souscription_min'], 0, '', ' ') . " €";
        echo "<br/>";
        echo "<span class=\"blank_line\">&nbsp;</span>";
        echo "</div>";
        echo "<div class=\"col-12 col-lg-3\">";
        echo "<span class=\"gras\">Date de création : </span>";
        if($donnees['date_creation'] != "1900-01-01" && $donnees['date_creation'] != "3000-01-01"){
            echo formatdate($donnees['date_creation']);
        }
        echo "<br/>";
        if($donnees['duree_min_de_placement'] > 0){
            echo "<span class=\"gras\">Durée min de placement : </span>";
            if($donnees['duree_min_de_placement'] != 100){
                echo $donnees['duree_min_de_placement'] . " ans";
            }
            echo "<br/>";
            echo "<span class=\"gras\" style=\"visibility : hidden\">F</span>";
        }else{
            echo "<span class=\"gras\">Fin de souscription : </span>";
            if($donnees['fin_souscription'] != "2100-01-01" && $donnees['fin_souscription'] != "3000-01-01"){
                echo formatdate($donnees['fin_souscription']);
            }
            echo "<br/>";
            echo "<span class=\"gras\">Date de déblocage : </span>";
            if($donnees['date_deblocage'] != "3000-01-01" && $donnees['date_deblocage'] != "1900-01-01"){
                echo formatdate($donnees['date_deblocage']);
            }
            echo "<br/>";
            echo "<span class=\"blank_line\">&nbsp;</span>";
        }
        echo "</div>";
        echo "<div class=\"col-12 col-lg-6\">";
        echo "<span class=\"gras\"> Secteur : </span>";
        if($donnees['secteur'] != "Inconnu"){
            echo $donnees['secteur'];
        }
        echo "<br/>";
        echo "<span class=\"gras\"> Géographie : </span>";
        if($donnees['geographie'] != "Inconnu"){
            echo $donnees['geographie'];
        }
        echo "<br/>";
        echo "<span class=\"gras\"> Profil de risque et de rendement : </span>" . $donnees['profil_risque'];
        echo "</div>";
        echo "</div>";
        echo "</div>";
        echo "</div>";
    }


}
$req->closeCursor();
