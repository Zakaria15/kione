-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 20 jan. 2021 à 14:32
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `kione`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date_article` date NOT NULL,
  `contenu` text NOT NULL,
  `afficher` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`id`, `titre`, `image`, `date_article`, `contenu`, `afficher`) VALUES
(1, 'Le private Equity', 'vign4.jpg', '2020-11-11', 'Fin diam. Integer tincidunt, tellus id consequat sollicitudin, urna magna convallis erat, ac bibendum eros ligula sed velit. Nam sodales rhoncus congue. Integer et consectetur arcu, ut mattis ex. Sed in libero ante. Nam nec lacus justo. Duis elementum diam nec leo semper euismod. Vestibulum sollicitudin et odio interdum posuere. Aliquam ultrices orci non dui venenatis ultricies. Pellentesque gravida metus in nulla dignissim lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.                                                                                                                                                                                                ', 1),
(2, 'Les FCPR', 'vign5.jpg', '2020-11-11', 'Buis dignissim finibus diam. Integer tincidunt, tellus id consequat sollicitudin, urna magna convallis erat, ac bibendum eros ligula sed velit. Nam sodales rhoncus congue. Integer et consectetur arcu, ut mattis ex. Sed in libero ante. Nam nec lacus justo. Duis elementum diam nec leo semper euismod. Vestibulum sollicitudin et odio interdum posuere. Aliquam ultrices orci non dui venenatis ultricies. Pellentesque gravida metus in nulla dignissim lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 1),
(3, 'Le futur de l\'investissement', 'vign6.jpg', '2020-11-10', 'Cuis dignissim finibus diam. Integer tincidunt, tellus id consequat sollicitudin, urna magna convallis erat, ac bibendum eros ligula sed velit. Nam sodales rhoncus congue. Integer et consectetur arcu, ut mattis ex. Sed in libero ante. Nam nec lacus justo. Duis elementum diam nec leo semper euismod. Vestibulum sollicitudin et odio interdum posuere. Aliquam ultrices orci non dui venenatis ultricies. Pellentesque gravida metus in nulla dignissim lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 1),
(4, 'Soutenez les localités', 'vign7.jpg', '2020-11-10', 'Duis dignissim finibus diam. Integer tincidunt, tellus id consequat sollicitudin, urna magna convallis erat, ac bibendum eros ligula sed velit. Nam sodales rhoncus congue. Integer et consectetur arcu, ut mattis ex. Sed in libero ante. Nam nec lacus justo. Duis elementum diam nec leo semper euismod. Vestibulum sollicitudin et odio interdum posuere. Aliquam ultrices orci non dui venenatis ultricies. Pellentesque gravida metus in nulla dignissim lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 1);

-- --------------------------------------------------------

--
-- Structure de la table `fonds`
--

DROP TABLE IF EXISTS `fonds`;
CREATE TABLE IF NOT EXISTS `fonds` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `id_gestion` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `date_creation` date NOT NULL,
  `decouvrir` varchar(255) NOT NULL,
  `souscription_en_ligne` tinyint(1) NOT NULL,
  `souscrire` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `secteur` varchar(255) NOT NULL,
  `geographie` varchar(255) NOT NULL,
  `souscription_min` int(11) NOT NULL,
  `frais_de_gestion` float NOT NULL,
  `date_deblocage` date NOT NULL,
  `reduction_ir` float NOT NULL,
  `fin_souscription` date NOT NULL,
  `duree_min_de_placement` int(11) NOT NULL,
  `profil_risque` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `fonds`
--

INSERT INTO `fonds` (`ID`, `id_gestion`, `nom`, `date_creation`, `decouvrir`, `souscription_en_ligne`, `souscrire`, `contact`, `secteur`, `geographie`, `souscription_min`, `frais_de_gestion`, `date_deblocage`, `reduction_ir`, `fin_souscription`, `duree_min_de_placement`, `profil_risque`) VALUES
(1, '1', 'FIP SOLIDAIRE MAIF 2020', '2020-10-23', 'https://www.maif.fr/epargne-patrimoine/conseil-patrimonial/fip-solidaire-maif', 0, '', '', 'Hôtèlerie', 'France', 1000, 0.48, '2027-12-30', 22.5, '2021-12-31', 0, 7),
(2, '2', 'ALIMEA CORSE AMBITION 2026\r\n', '2019-10-18', 'https://www.sigmagestion.com/solutions/solutions-ouvertes-souscription/fip-corse-ambition-2026/', 1, 'https://www.sigmagestion.com/wp-content/uploads/2019/11/BS-dynamique-FIP-CORSE-ALIMEA-AMBITION-2026-sigma-direct.pdf', 'infos@sigmagestion.com - 01 47 03 64 59', 'Energies renouvelables; Tourisme; Agroalimentaire; Santé; Service', 'Corse', 1000, 3.66, '2026-12-31', 38, '2020-12-31', 0, 0),
(3, '3', 'PME 974 N°6', '2019-10-18', '', 0, '', '+33 (0)1 82 28 18 00', 'Energie; Tourisme; Agroalimentaire; Télécommunications; Infrastructure', 'Outre-Mer', 1000, 3.86, '2026-12-31', 21, '2021-02-28', 0, 0),
(4, '4', 'SUMINA N°4', '2019-10-15', 'https://static1.squarespace.com/static/59ad26afcd39c3b55fa20c14/t/5f92fe2382e8af1f62b58472/1603468837316/Sumina4_Plaquette_A4.pdf', 0, '', 'aio@femuqui.com / +33 4 95 31 59 46', 'Agroalimentaire; Numérique; Tourisme; Mobilité; Distribution; Service', 'Corse', 500, 3, '2027-12-31', 27, '2020-12-31', 0, 7),
(5, '5', 'FIP ALLIANCE 2019', '2019-09-20', 'https://www.ixope.fr/fonds_geres/fip-apl-2019/', 0, '', '+ 33 (0)1 70 62 94 30 - +33 1 70 62 95 27', 'Economie', 'France; Corse; Outre-Mer', 500, 3, '2027-12-31', 20, '2020-01-31', 0, 7),
(16, '13', 'FCPR APAX PRIVATE EQUITY OPPORTUNITIES', '2020-09-25', 'https://www.apax.fr/2020/11/03/apax-partners-lance-apax-private-equity-opportunities-apeo/', 0, 'Tech; Consumer; Santé; Services', '', 'Inconnu', 'Inconnu', 0, 0, '1900-01-01', 0, '2100-01-01', 100, 0),
(6, '6', 'FIP APL 2019', '2019-09-17', 'https://www.ixope.fr/wp-content/uploads/2019/10/Plaquette_FIP_APL_2019.pdf', 0, '', 'contact@ixope.fr - 05 34 417 418', 'Aucun particulier', 'France', 1000, 3.11, '2026-12-31', 18, '2021-12-25', 0, 0),
(7, '7', 'FIP KALLISTE CAPITAL N°12', '2019-09-10', '', 0, '', '01 40 15 61 77 / CONTACT@VATELCAPITAL.COM', 'Santé; Tourisme; Energies renouvelables; Service; Agroalimentaire', 'Corse', 1000, 3.89, '2026-12-31', 38, '2020-09-30', 0, 7),
(8, '8', 'FIP ODYSSEE PME CROISSANCE N°5', '2019-08-09', 'http://www.odysseeventure.com/souscrire-fip-en-ligne-2/', 0, '', '01 71 18 11 50', 'Aucun particulier', 'France', 3000, 3.89, '2026-10-31', 18, '2020-12-31', 0, 7),
(9, '9', 'FIP GALIA PME 2019', '2019-08-06', 'https://www.galia-gestion.com/infos-souscripteurs-fip/informations-financieres-et-documentation-fip-galia-pme-2019', 0, '', '33 (0)5 57 81 88 10', 'Aucun particulier', 'France; Outre-Mer', 500, 3.49, '2027-12-31', 20, '2021-12-28', 0, 7),
(10, '10', 'FIP FRANCE ENTREPRENDRE 2019', '2019-08-02', 'https://www.bred.fr/banqueprivee/articles/defiscalisation-ir-2019/FIP%20France%20Entreprendre%202019.pdf', 0, '', '+33 1 49 26 03 10', 'Aucun particulier', 'France', 2000, 3.49, '2026-12-31', 18, '2019-12-31', 0, 7),
(11, '11', 'NEOVERIS CORSE 2019', '2019-05-24', '', 0, '', '04 91 29 41 71', 'Aucun particulier', 'Corse', 1000, 3.5, '2027-12-31', 38, '2020-07-31', 0, 7),
(12, '12', 'FIP OUTRE-MER INTER INVEST N°2', '2019-04-26', 'https://www.inter-invest.fr/tools/documentOff/doc/brochure-fip-outre-mer-inter-invest-2.pdf', 0, '', 'contact@interinvestcapital.fr / 01 56 62 00 55', 'Aucun particulier', 'Outre-Mer', 1000, 3.5, '2027-06-30', 38, '2020-06-30', 0, 7),
(13, '11', 'NEOVERIS CORSE 2020', '2020-10-27', 'https://www.smaltcapital.com/fip-neoveris-corse-2020', 0, '', 'service-client@smaltcapital.com', 'Aucun particulier', 'Corse', 1000, 3.5, '2028-12-31', 30, '2021-12-27', 0, 0),
(14, '7', 'FIP Kallisté Capital n°13', '2020-09-18', 'https://www.vatelcapital.com/souscrire-fcpioufip/fipcorse-2/', 0, '', '01 40 15 61 77', 'Aucun particulier', 'Corse', 1000, 3.49, '2027-12-31', 30, '2020-12-31', 0, 7),
(15, '7', 'FIP Mascarin Capital N°3', '2020-09-18', 'https://www.vatelcapital.com/souscrire-fcpioufip/fip-mascarin-capital-2/', 0, '', '01 40 15 61 77', 'Tourisme; Energies renouvelables; Agroalimentaire; Service', 'Outre-Mer', 1000, 3.49, '2027-12-31', 30, '2020-12-31', 0, 7),
(17, '14', 'AXA AVENIR ENTREPRENEURS', '2020-06-16', '', 0, '', '', 'Aucun particulier', 'Europe', 75000, 4.41, '1900-01-01', 0, '2100-01-01', 8, 7),
(18, '15', 'AXA AVENIR INFRASTRUCTURE', '2020-06-12', 'https://www.axa.fr/epargne-retraite/assurance-vie/avenir-infrastructure.html', 0, '', '', 'Transport; Social; Energie; Eau; Gestion des déchets; Energie; Télécommunications', 'Europe', 75000, 0, '1900-01-01', 0, '2100-01-01', 0, 7),
(19, '16', 'FCPR ENTREPRENEURS & RENDEMENT N°5', '2020-06-05', '', 0, '', 'cosson@entrepreneurinvest.com / +33 7 84 07 95 64', 'Aucun particulier', 'Europe', 1000, 3.84, '2026-12-31', 0, '2020-12-31', 0, 7),
(20, '17', 'FCPR WHITE CAPS SELECTION 2', '2020-04-21', 'https://www.lbofrance.com/wp-content/uploads/2020/06/Plaquette_White-Caps-S%C3%A9lection-2.pdf', 0, '', '+33 1 40 62 77 67', 'Aucun particulier', 'Europe', 25000, 4.26, '2028-09-30', 0, '2021-12-02', 0, 0),
(21, '18', 'ARDIAN MULTI STRATÉGIES FCPR', '2020-03-06', '', 0, '', '+33 1 41 71 92 00', 'Aucun particulier', 'Inconnu', 0, 3.07, '2030-03-06', 0, '2021-09-06', 0, 0),
(22, '1', 'HOTELS SELECTION EUROPE N°3\r\n', '2020-01-21', 'https://extendam.com/wp-content/uploads/2020/02/Brochure-FCPR-HSE-3_bdf-3.pdf', 0, '', '+33 1 53 96 52 50', 'Hôtèlerie', 'Europe', 5000, 4.29, '2026-12-31', 0, '2020-12-31', 0, 0),
(23, '16', 'FCPR ENTREPRENEURS ET IMMOBILIER', '2019-12-20', '', 0, '', '+33 1 58 18 61 80', 'Immobilier', 'Europe', 1000, 2.45, '1900-01-01', 0, '2100-01-01', 3, 0),
(24, '19', 'IDINVEST HEC VENTURE FUND', '2020-12-20', 'https://www.hecventures.com/', 1, 'https://hec-ventures.idinvest.com/users/sign_up', 'aloiseau@idinvest.com', 'Télécommunications', 'Europe', 10000, 2.66, '2029-12-31', 0, '2021-06-30', 0, 0),
(25, '20', 'SELECT PROMOTION II', '2019-02-03', 'https://www.mcapital.fr/investir/fcpr-select-promotion-ii/', 1, 'https://www.mcapital.fr/wp-content/uploads/KIT-FCPR-SELECT-PROMOTION-II.pdf', '+33 5 34 32 09 65', 'Immobilier', 'France', 5000, 4.41, '2024-09-30', 0, '2020-09-30', 0, 7),
(26, '21', 'FCPR AMUNDI PRIVATE EQUITY MEGATENDANCES II', '2019-10-18', 'https://www.amundi.fr/fr_part/product/view/FR0013430378', 0, '', '', 'Santé; Agroalimentaire; Energie; Eau; Urbanisation; Environnement; Télécommunications; Globalisation', 'Europe', 100, 4.9, '2027-10-18', 0, '2021-06-30', 0, 7),
(27, '19', 'FCPR IDINVEST ENTREPRENEURS CLUB', '2019-07-19', 'https://espace-particuliers.idinvest.com/fr/nos-fonds/fcpr-idinvest-entrepreneurs-club/', 0, '+33 1 58 18 56 56', '', 'Télécommunications; Numérique; Santé', 'Europe', 20000, 2.94, '2029-12-31', 0, '2022-12-31', 0, 7),
(28, '19', 'IDINVEST STRATEGIC OPPORTUNITIES 2', '2019-05-28', 'https://espace-particuliers.idinvest.com/fr/nos-fonds/fcpr-idinvest-strategic-opportunities-2/', 0, '', '+ 33 1 58 18 56 56', 'Industrie; Service', 'Europe', 10000, 4.44, '2027-12-31', 0, '2020-12-31', 0, 7),
(29, '22', 'FCPR NEXTSTAGE RENDEMENT II', '2019-05-17', 'https://www.nextstage-am.com/nv-slider/fcpr-nextstage-rendement-ii/', 0, '', 'partenaires@nextstage.com - +33 1 44 29 99 07', 'Aucun particulier', 'Europe', 3000, 4, '2026-06-30', 0, '2020-06-30', 0, 7),
(30, '23', 'FRANCE PROMOTION 2019', '2019-04-26', 'https://123-im.com/investir/fcpr-france-promotion-2019/', 1, 'https://123-im.com/wp-content/uploads/2020/07/Dossier-de-souscription-France-Promotion-2019-PP-PM-CGP-r.pdf', '+ 33 1 49 26 98 00', 'Immobilier', 'Europe', 5000, 4.73, '2025-06-30', 0, '2020-12-31', 0, 7),
(31, '23', '123CORPORATE 2019', '2019-03-15', 'https://123-im.com/investir/fcpr-123-corporate-2019/', 1, '', '+ 33 1 49 26 98 00', 'Tourisme; Santé; Education; Immobilier', 'Europe', 5000, 4.2, '2026-03-15', 0, '2020-03-15', 0, 0),
(32, '16', 'FCPR ENTREPRENEURS ET RENDEMENT N°4', '2019-01-11', '', 0, '', '', 'Inconnu', 'Inconnu', 0, 0, '1900-01-01', 0, '2100-01-01', 0, 0),
(33, '24', 'FCPR Audacia Entrepreneuriat Familial', '2020-08-07', 'https://www.audacia.fr/2020/09/02/fcpr-audacia-entrepreneuriat-familial/', 0, '', 'contact@audacia.fr / +33 1 56 43 48 00', 'Aucun particulier', 'Europe', 5000, 3.8, '1900-01-01', 0, '2100-01-01', 6, 7),
(35, '8', 'FCPR ODYSSEE ACTIONS', '1900-01-01', 'http://www.odysseeventure.com/souscription-fcpr-en-ligne/', 0, '', '+33 1 71 18 11 50', 'Aucun particulier', 'France; Europe', 10000, 4.98, '2027-09-30', 0, '2022-03-31', 0, 7),
(36, '23', '123 CORPORATE 2020', '2020-06-09', 'https://123-im.com/investir/fcpr-123-corporate-2020#toggle-id-4', 0, '', '+ 33 1 49 26 98 00', 'Tourisme; Santé; Education; Immobilier', 'France; Europe', 5000, 4.6, '2027-06-09', 0, '2021-06-09', 0, 0),
(34, '8', 'FIP ODYSSEE PME CROISSANCE N°6', '2020-08-11', 'http://www.odysseeventure.com/souscription-fip-en-ligne/', 0, '', 'souscripteurs@odysseeventure.com', 'Aucun particulier', 'France', 3000, 3.89, '2027-10-31', 18, '2021-12-31', 0, 7),
(37, '23', 'BPIFRANCE ENTREPRISES 1', '2020-09-01', 'https://123-im.com/investir/fcpr-bpifrance-entreprises-1#toggle-id-3', 0, '', 'fonds-bpi@123-im.com', 'Aucun particulier', 'France', 5000, 3.92, '2026-10-01', 0, '2021-09-30', 0, 0),
(38, '19', 'FCPR IDINVEST PRIVATE VALUE EUROPE 3', '2018-03-13', 'https://espace-particuliers.idinvest.com/fr/nos-fonds/fcpr-idinvest-private-value-europe-3', 0, '', 'contact@idinvest.com / 01 58 18 56 56', 'Industrie; Service', 'France; Europe', 20000, 3.09, '1900-01-01', 0, '2100-01-01', 4, 7),
(39, '7', 'FCPI DIVIDENDES PLUS N°8', '2020-09-18', 'https://www.vatelcapital.com/souscrire-fcpioufip/fcpi-dividendes-plus-8-3/', 0, '', 'contact@vatelcapital.com / 01 40 15 61 77', 'Innovation; Santé; Numérique', 'France', 1000, 3.49, '2026-12-31', 25, '2020-12-31', 0, 7),
(40, '25', 'FCPI ALTO INNOVATION 2020', '2020-07-21', 'https://www.eiffel-ig.com/particuliers/fonds/FCPI-ALTO-INNOVATION-2020', 0, '', '+ 33 1 39 54 35 67', 'Télécommunications; Sciences de la vie', 'Europe', 1500, 3.89, '2028-01-01', 22, '2021-01-01', 0, 7),
(41, '21', 'FCPI AMUNDI AVENIR INNOVATION II', '2019-11-05', '', 0, '', '', 'Inconnu', 'Inconnu', 0, 0, '1900-01-01', 0, '2100-01-01', 0, 0),
(42, '19', 'OBJECTIF INNOVATION 2019', '2019-08-30', 'https://espace-particuliers.idinvest.com/fr/nos-fonds/fcpi-objectif-innovation-2019', 0, '', '', 'Télécommunications; Santé; Environnement', 'Inconnu', 0, 0, '1900-01-01', 0, '2100-01-01', 0, 0),
(43, '19', 'FCPI IDINVEST PATRIMOINE 2019', '2019-08-30', 'https://espace-particuliers.idinvest.com/fr/nos-fonds/fcpi-idinvest-patrimoine-2019', 0, '', '', 'Télécommunications; Santé; Environnement', 'France; Europe', 1000, 3.89, '2026-12-31', 18, '2019-12-31', 0, 7),
(44, '26', 'ISATIS EXPANSION N°6', '2019-08-27', 'https://isatis-capital.fr/fonds/isatis-expansion-n6', 0, '', '', 'Inconnu', 'Inconnu', 1000, 0, '2026-12-31', 18, '2020-03-31', 0, 0),
(45, '25', 'FCPI ALTO INNOVATION 2019', '2019-08-13', 'https://www.eiffel-ig.com/particuliers/fonds/FCPI-ALTO-INNOVATION-2019', 0, '', '', 'Télécommunications; Sciences de la vie', 'France; Europe', 1500, 3.89, '2027-01-01', 0, '2019-12-31', 0, 0),
(46, '27', 'SIPAREX XANGE INNOVATION 2020', '2019-08-09', 'https://www.siparexfondsfiscaux.com/fonds/siparex-xange-innovation-2020/', 0, '', 'innovation@siparex.com / 01 53 93 00 90', 'Numérique', 'Inconnu', 500, 0, '2026-12-31', 18, '2019-12-31', 0, 7),
(47, '18', 'ARDIAN ENTREPRENEURS & CROISSANCE 2019', '2019-08-09', '', 0, '', '', 'Inconnu', 'Inconnu', 0, 0, '1900-01-01', 0, '2100-01-01', 0, 0),
(48, '28', 'FCPI AVENIR NUMERIQUE 1', '2019-08-06', 'https://geco.amf-france.org/Bio/Bio/BIO_PDFS/NIP_NOTICE_PRODUIT/336999.pdf', 0, '', '01 44 94 15 00', 'Télécommunications; Numérique', 'France; Europe', 1000, 3.49, '2027-12-31', 0, '2020-10-06', 0, 7),
(49, '22', 'FCPI NEXTSTAGE CAP 2026', '2019-07-19', 'https://geco.amf-france.org/Bio/Bio/BIO_PDFS/NIP_NOTICE_PRODUIT/335892.pdf', 0, '', 'partenaires@nextstage.com / 01 44 29 99 07', 'Inconnu', 'Inconnu', 0, 0, '1900-01-01', 22.5, '2020-12-31', 0, 0),
(50, '29', 'TRUFFLE INNOVATION 2019', '2019-06-28', '', 0, '', '', 'Inconnu', 'Inconnu', 1000, 0, '1900-01-01', 18, '2019-12-31', 0, 7),
(51, '22', 'FCPI UFF FRANCE INNOVATION N°2', '2019-06-04', 'https://www.uff.net/espace-client/documentations//FR0013419090_prospectus.pdf', 0, '', 'info@nextstage.com / 01 53 93 49 40', 'Aucun particulier', 'France; Europe', 0, 4.13, '2027-03-31', 0, '2021-05-31', 0, 7),
(52, '30', 'CAPITAL INVEST PME 2019', '2019-05-24', 'https://portail-lcl-web.cdn.prismic.io/portail-lcl-web%2Fd7cbbe8b-be34-4c60-bef1-16d4625d1d6d_depliant-fcpi-capital-invest-pme-2019.pdf', 0, '', '', 'Sciences de la vie; Télécommunications', 'Inconnu', 2000, 3.76, '2027-12-30', 18, '2019-12-24', 0, 0),
(53, '2', 'FCPI FRANCE EVOLUTION', '2020-09-29', 'https://www.sigmagestion.com/france-evolution/', 0, '', '', 'Santé; Télécommunications; Agroalimentaire', 'Inconnu', 1000, 3.66, '2027-12-31', 25, '2020-12-31', 0, 7),
(54, '16', 'ENTREPRENEURS ET OPPORTUNITES', '1900-01-01', 'https://www.entrepreneurinvest.com/produits/', 0, '', '', 'Inconnu', 'Inconnu', 0, 0, '1900-01-01', 0, '2100-01-01', 0, 0),
(55, '16', 'ENTREPRENEURS ET CROISSANCE', '1900-01-01', 'https://www.entrepreneurinvest.com/produits/', 0, '', '', 'Inconnu', 'Inconnu', 0, 0, '1900-01-01', 0, '2100-01-01', 0, 0),
(56, '16', 'ENTREPRENEURS ET INNOVATION', '2020-10-13', '', 0, '', '01 58 18 61 80', 'Education; E-commere; Numérique', 'France; Europe', 1000, 3.69, '2026-12-31', 24, '2020-12-31', 0, 7),
(57, '22', 'FCPI NextStage Découvertes 2020-2021', '2020-09-29', 'http://www.nextstage-am.com/fr/solutions-dinvestissement/fonds-investisseurs-prives/', 0, '', 'partenaires@nextstage-am.com / 01 44 29 99 07', 'Transition énergétique; Santé; Sécurité; Numérique', 'France', 3000, 3.8, '2028-12-31', 23, '2021-12-31', 0, 0),
(58, '26', 'ISATIS EXPANSION N°7', '1900-01-01', 'https://www.maif.fr/particuliers/conseil-patrimonial/nos-solutions-financieres-et-immobilieres/fcpi-isatis-expansion.html', 0, '', 'fcpi@isatis-capital.info', 'Numérique; Santé; Industrie', 'Inconnu', 1000, 3.9, '2027-12-31', 22.5, '2022-02-28', 0, 7),
(59, '30', 'CAPITAL INVEST PME 2020', '1900-01-01', 'https://portail-lcl-web.cdn.prismic.io/portail-lcl-web/e346e78b-0c4e-4f1a-b5bc-4426c7910503_PART_BP_Div_Pat_FCPI_Capital_Invest_PME_2020_Brochure_BdP.pdf', 0, '', '', 'Santé; Télécommunications', 'France; Europe', 2000, 3.89, '2028-12-30', 20, '2020-12-24', 0, 0),
(60, '24', 'Audacia PME Croissance', '2020-09-04', 'https://www.audacia.fr/2020/09/02/audacia-pme-croissance/', 1, 'https://audacia.mipise.fr/fr/users/sign_in?return_to=https%3A%2F%2Faudacia.mipise.fr%2Ffr%2Fprojects%2Fpme_croissance%2Fbackers%2Fnew&session_or_registration=true', 'investir@audacia.fr / 01 56 43 48 00', 'Aucun particulier', 'France', 1000, 3.33, '2028-12-31', 25, '2020-12-21', 0, 7);

-- --------------------------------------------------------

--
-- Structure de la table `gestionnaires`
--

DROP TABLE IF EXISTS `gestionnaires`;
CREATE TABLE IF NOT EXISTS `gestionnaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `date_agrement` date NOT NULL,
  `img` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `gestionnaires`
--

INSERT INTO `gestionnaires` (`id`, `nom`, `url`, `date_agrement`, `img`) VALUES
(1, 'Extendam', 'www.extendam.com', '2013-01-02', 'extendam.png'),
(2, 'Sigma gestion', 'www.sigmagestion.com', '2004-07-23', 'sigmagestion.png'),
(3, 'Apicap', 'www.apicap.fr', '2001-08-16', 'apicap.png'),
(4, 'Femu quì ventures', 'ventures.femuqui.com', '2016-06-02', 'femuquiventures.png'),
(5, 'Alliance entreprendre', 'www.allianceentreprendre.com', '2002-10-31', 'allianceentreprendre.png'),
(6, 'Ixo private equity', 'www.ixope.fr', '2003-07-15', 'ixoprivateequity.png'),
(7, 'Vatel capital', 'www.vatelcapital.com', '2008-09-30', 'vatelcapital.png'),
(8, 'Odyssee venture', 'www.odysseeventure.com', '1999-12-01', 'odysseeventure.png'),
(9, 'Galia gestion', 'www.galia-gestion.com', '2002-11-30', 'galiagestion.png'),
(10, 'Generis capital partners sas', 'www.generiscapital.com', '2008-09-26', 'generiscapitalpartners.png'),
(11, 'Smalt capital', 'www.smaltcapital.com', '2000-10-20', 'smaltcapital.png'),
(12, 'Inter invest capital', 'www.interinvestcapital.fr', '2015-02-25', 'interinvestcapital.png'),
(13, 'Apax partners sas', 'www.apax.fr', '2008-12-19', 'apaxpartnerssas.png'),
(14, 'Axa investment managers paris', 'www.axa-im.fr', '1992-04-07', 'axa.png'),
(15, 'Axa reim sgp', 'www.axa-reimsgp.fr', '2008-05-05', 'axa.png'),
(16, 'Entrepreneur invest', 'www.entrepreneurinvest.com', '2000-05-30', 'entrepreneurinvest.png'),
(17, 'Lbo france gestion', 'www.lbofrance.com', '1998-02-27', 'lbofrance.png'),
(18, 'Ardian france', 'www.ardian.com/fr', '1999-12-06', 'ardian.png'),
(19, 'Idinvest partners', 'www.idinvest.com', '1999-06-10', 'idinvestpartners.png'),
(20, 'M capital partners', 'www.mcapital.fr', '2002-10-29', 'mcapital.png'),
(21, 'Amundi private equity funds', 'www.amundi.fr', '1999-05-21', 'amundi.png'),
(22, 'Nextstage am', 'www.nextstage.com', '2002-07-09', 'nextstageam.png'),
(23, '123 investment managers', 'www.123-im.com', '2001-06-28', '123investmentmanagers.png'),
(24, 'Audacia', 'www.audacia.fr', '2009-10-20', 'audacia.png'),
(25, 'Eiffel investment group', 'www.eiffel-ig.com', '2010-09-01', 'eiffel-ig.png'),
(26, 'Isatis capital', 'www.isatis-capital.fr', '2013-07-31', 'isatiscapital.png'),
(27, 'Siparex xange venture', 'www.siparexfondsfiscaux.com', '2004-04-27', 'siparex.png'),
(28, 'Innovacom gestion', 'www.innovacom.com', '2000-05-26', 'innovacom.png'),
(29, 'Truffle capital s.a.s', 'www.truffle.com', '2001-07-24', 'trufflecapital.png'),
(30, 'Omnes capital', 'www.omnescapital.com/fr', '2000-08-28', 'omnescapital.png');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
