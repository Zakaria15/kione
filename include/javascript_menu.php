<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script>
$( document ).ready(function() {
  // Initial scroll position
  var scrollState = 0;

  // Store navbar classes
  var navClasses1 = document.getElementById('navBar1').classList;
  var navClasses2 = document.getElementById('navBar2').classList;

  // returns current scroll position
  var scrollTop = function() {
    return window.scrollY;
  };

  // Primary scroll event function
  var scrollDetect = function(home, down, up) {
    // Current scroll position
    var currentScroll = scrollTop();
    if (scrollTop() === 0) {
      up();
    } else if (scrollTop !== 0) {
      down();
    } else {
      up();
    }
    // Set previous scroll position
    scrollState = scrollTop();
  };

  function homeAction() {
    console.log("home");
  }

  function downAction() {
    navClasses1.add('collapse','animate__animated','animate__fadeInDown');
    navClasses1.remove('open','animate__animated','animate__fadeInDown');

    navClasses2.add('open','animate__animated','animate__fadeInDown');
    navClasses2.remove('collapse','animate__animated','animate__fadeInDown');
  }

  function upAction() {
    navClasses1.add('open','animate__animated','animate__fadeInDown');
    navClasses1.remove('collapse','animate__animated','animate__fadeInDown');

    navClasses2.add('collapse','animate__animated','animate__fadeInDown');
    navClasses2.remove('open','animate__animated','animate__fadeInDown');
  }

  $(".menu_item_1").each(function(){
    $(this).hover(function(){
      $(this).css('color','#70ad47');
      $(this).css('background-color', 'white');
      $(this).next().css('display', 'block');
      $(this).css('cursor','pointer');

    });
  });

  $(".menu_item_1").each(function(){
    $(this).mouseleave(function(){
      $(this).css('color','white');
      $(this).css('background-color', '#70ad47');
      $(this).next().css('display', 'none');
    });
  });

  $(".sous_menu_1").each(function(){
    $(this).hover(function(){
      $(this).prev().css('background-color', 'white');
      $(this).prev().css('color', '#70ad47');
      $(this).css('display', 'block');
    });
  });

  $(".sous_menu_1").each(function(){
    $(this).mouseleave(function(){
      $(this).css('display', 'none');
      $(this).prev().css('background', '#70ad47');
      $(this).prev().css('color', 'white');
    });
  });

  $(".menu_item_2").each(function(){
    $(this).hover(function(){
      $(this).next().css('display', 'block');
      $(this).css('cursor','pointer');
    });
  });

  $(".menu_item_2").each(function(){
    $(this).mouseleave(function(){
      $(this).next().css('display', 'none');
    });
  });

  $(".sous_menu_2").each(function(){
    $(this).hover(function(){
      $(this).css('display', 'block');
    });
  });

  $(".sous_menu_2").each(function(){
    $(this).mouseleave(function(){
      $(this).css('display', 'none');
    });
  });

  window.addEventListener("scroll", function() {
    scrollDetect(homeAction, downAction, upAction);
  });

});
</script>
