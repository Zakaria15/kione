<header id="navBar1">
  <div class="row wrapper nopnom">
    <div class="col-12 ajuster nopnom">
      <a href="index.php" src="En-tête du site" class="en_tete">
        <span class="titre_header">Investissement-<span style="color : #70ad47;">utile</span>.fr</span>
        <br/>
        <span class="sous_titre_header">Investissez dans les entreprises de demain</span></a>
      </div>
    </div>
    <div class="nav">
      <div class="wrapper">

        <div class="nopnom">
          <div class="menu_item_1">
            Tout comprendre
          </div>

          <div class="sous_menu_1">
            <div><a href="">Les investissements non côtés</a></div>
            <div><a href="">Qu'est-ce que les FCPR ?</a></div>
            <div><a href="">Exemple de sociétés financées</a></div>
          </div>
        </div>

        <div class="nopnom">
          <div class="menu_item_1">
            Investir
          </div>
          <div class="sous_menu_1">
            <div><a href="pourquoi_investir.php">Pourquoi</a></div>
            <div><a href="">Quels sont les risques</a></div>
            <div><a href="">Comment souscrire ?</a></div>
          </div>
        </div>
        <div class="">
          <div class="menu_item_1">
            <a href="liste_fonds.php">Liste des fonds</a>
          </div>
          <div class="sous_menu_1">
          </div>
        </div>
      </div>

    </header>

    <div id="navBar2" class="header2">

      <div class="row wrapper" style="margin-right : 0 !important;">
        <div class="nopnom ajuster" style="margin-right: 10%;">

          <a href="index.php" src="En-tête du site" class="en_tete">

            <span class="titre_header">Investissement-<span style="color : #70ad47;">utile</span>.fr</span>
            <br/>
            <span class="sous_titre_header">Investissez dans les entreprises de demain</span>

        </a>
          </div>
          <div class="nopnom">
            <div class="menu_item_2" style="color : #70ad47; height : 100%; display : flex; flex-direction : column; justify-content : space-evenly;">
              <div>Tout comprendre</div>
            </div>
            <div class="sous_menu_2">
              <div><a href="">Les investissements non côtés</a></div>
              <div><a href="">Qu'est-ce que les FCPR ?</a></div>
              <div><a href="">Exemple de sociétés financées</a></div>
            </div>
          </div>
          <div class="nopnom" style="margin-left : 10%;">
            <div class="menu_item_2" style="color : #70ad47; height : 100%; display : flex; flex-direction : column; justify-content : space-evenly; ">
              <div>Investir</div>
            </div>
            <div class="sous_menu_2">
                <div><a href="pourquoi_investir.php">Pourquoi</a></div>
                <div><a href="">Quels sont les risques</a></div>
                <div><a href="">Comment souscrire ?</a></div>
            </div>
          </div>
          <div class="menu_section" style="display : flex; flex-direction : column; justify-content : space-evenly; margin-left : 10%; color : #70ad47;">
            <div class="menu_item_2">
              <a href="liste_fonds.php">Liste des fonds</a>
            </div>
          </div>
        </div>

      </div>
