<div class="row" style="display : flex; justify-content: space-around;">

    <div class="actualites_container">
        <div class="row" style="width : 100%; display : flex; justify-content : center; border-bottom : 1px solid #70ad47;">
            <div class="titre_actualites" style="color : white; background-color : #70ad47; padding: 1% 1%;">Actualités</div>
        </div>
        <?php
        $req=$bdd->query('SELECT * FROM articles WHERE afficher = 1 ORDER BY RAND() LIMIT 3');
        while($donnees=$req->fetch()){ ?>
            <div class="row" style="display : flex; justify-content: center; width : 75%; display : flex; padding : 2%;">
                <div class="image_article">
                    <a href="article.php?id=<?= $donnees['id'] ?>"/><div style=""><img src="images/articles/<?php echo $donnees['image']?>" alt=""/></div></a>
                </div>
                <div class="" style="width : 50%; display : flex; flex-direction : column; justify-content: space-around;">
                    <a href="article.php?id=<?= $donnees['id'] ?>"/><div class="titre_actu" style="margin-left : 5%; text-decoration : underline;"><?= $donnees['titre']; ?></div></a>
                </div>

            </div>
            <?php
        }
        ?>
    </div>

</div>
