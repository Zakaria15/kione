<?php include('include/connexion_bdd.php');

function formate_date_to_lisible($date){
    $date_a_modif_split = explode('-', $date);
    $new_date = $date_a_modif_split[2] . '/' . $date_a_modif_split[1] . '/' . $date_a_modif_split[0];

    return $new_date;
}

if(!isset($_GET['id'])){
    header('Location: index.php');
}else{
    $id = $_GET['id'];
}

?>


<!DOCTYPE html>
<html>

<?php include('include/head.php'); ?>
<body>
    <?php
    include('include/header.php');
    include('include/navigation.php');
    $req = $bdd->prepare('SELECT * FROM articles WHERE id = ?');
    $req->execute(array($id));
    $donnees=$req->fetch();
    ?>

    <div style="margin-bottom : 5em; margin-top : 10em; margin-right : 15%; margin-left : 12%; width : 100%;">
        <div style="display : flex;">
        <div style="width : 50%;">
        <div style="width : 100%; margin-bottom : 0.3em; border-bottom : 1px solid #70ad47; font-size : 2em;"> <?= $donnees['titre'] ?> </div>
        <div style="width : 60%;"><img src="images/articles/<?= $donnees['image'] ?>" alt ="image article" style="width : 100%; height : auto;" /> </div>
        <div style="margin-bottom : 0.5em; color : grey;"> <?= formate_date_to_lisible($donnees['date_article']); ?> </div>
        <div style="text-align : justify; "> <?= $donnees['contenu']; ?> </div>
        </div>

        </div>

    <div style="display : flex; width : 100%;">
    <div style="width : 50%;">
        <div class="row" style="display : flex; justify-content : center; margin-top : 5em; margin-bottom : 3em;  width : 100%;">
            <div style="display : flex; justify-content : space-between; width : 70%;">
                <div class="vignette" style="height : 236px; width : 200px;">
                    <div style="height: 55%;">
                        <img src="images/vign1.png  " alt="" style="height : 100%; width : 100%;"/>
                    </div>
                    <div style="height: 45%;">
                        <div style="text-align : center; margin-top : 1em; width: 80%; margin-left :auto;margin-right :auto; height : 65%;  border-bottom : 0.5px solid #70ad47">
                            Qu’est ce que la finance non cotée ?
                        </div>
                    </div>
                </div>

                <div class="vignette" style="height : 236px; width : 200px;">
                    <div style="height: 55%;">
                        <img src="images/vign2.jpg  " alt="" style="height : 100%; width : 100%;"/>
                    </div>
                    <div style="height: 45%;">
                        <div style="text-align : center; margin-top : 1em; width: 75%; margin-left :auto;margin-right :auto; height : 65%;  border-bottom : 0.5px solid #70ad47">
                            Pourquoi investir dans le non cotée ?
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="display : flex; justify-content : center; margin-bottom : 3em;  width : 100%;">
            <div style="display : flex; justify-content : space-between; width : 70%;">
                <div class="vignette" style="height : 236px; width : 200px;">
                    <div style="height: 55%;">
                        <img src="images/vign3.jpg  " alt="" style="height : 100%; width : 100%;"/>
                    </div>
                    <div style="height: 45%;">
                        <div style="text-align : center; margin-top : 1em; width: 100%; margin-left :auto;margin-right :auto; height : 65%;">
                            Découvrir le fonctionnement d’un FCPR
                        </div>
                        <div style="margin-left : 10%; width : 80%; border-bottom : 0.5px solid #70ad47;">

                        </div>
                    </div>
                </div>

                <div class="vignette" style="height : 236px; width : 200px;">
                    <div style="height: 55%;">
                        <img src="images/vign4.jpg  " alt="" style="height : 100%; width : 100%;"/>
                    </div>
                    <div style="height: 45%;">
                        <div style="text-align : center; margin-top : 1em; width: 100%; margin-left :auto;margin-right :auto; height : 65%;">
                            Quels sont les risques d’investir dans un FCPR ?
                        </div>
                        <div style="margin-left : 10%; width : 80%; border-bottom : 0.5px solid #70ad47;">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="margin-top : -14vh; width : 40%; position: -webkit-sticky; position: sticky; top: 30%; height : 73vh;">
        <div class="row" style="display : flex; margin-left : 8%;">

            <div style="display : flex; flex-direction: column; align-items: center; width : 50%">
                <div class="row" style="width : 100%; display : flex; justify-content : center; border-bottom : 1px solid #70ad47;">
                    <div style ="color : white; background-color : #70ad47; padding: 1% 1%;">Autres articles</div>
                </div>
                <?php
                $req=$bdd->prepare('SELECT * FROM articles WHERE id <> ? AND afficher = 1 ORDER BY RAND()');
                $req->execute(array($id));
                while($donnees=$req->fetch()){ ?>
                    <div class="row" style="width : 80%; padding : 2%; margin-bottom : 2em;">
                        <div style="width : 100%;">
                            <a href="article.php?id=<?= $donnees['id'] ?>"/><div style=""><img src="images/articles/<?php echo $donnees['image']?>" alt=""/></div></a>
                        </div>
                        <div class="" style="width : 100%; display : flex; flex-direction : column; justify-content: space-around;">
                            <a href="article.php?id=<?= $donnees['id'] ?>"/><div class="titre_actu" style="margin-left : 5%; text-decoration : underline;"><?= $donnees['titre']; ?></div></a>
                        </div>

                    </div>
                    <?php
                }
                ?>
            </div>

        </div>
    </div>
</div>


    </div>





    <?php include('include/footer.php'); ?>
<?php include('include/javascript_menu.php'); ?>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>

$( document ).ready(function() {
    $('.vignette').each(function(){
        $(this).hover(function(){
            $(this).css('box-shadow','0px 1px 10px black');
            $(this).css('cursor','pointer');
        });
        $(this).mouseleave(function(){
            $(this).css('box-shadow','0px 0px 0px black');

        });

    });
$('.titre_actu').each(function(){
        $(this).hover(function(){
            $(this).css('color','#bb0b0b');
        });

        $(this).mouseleave(function(){
            $(this).css('color','black');
        });
    });
})
</script>
</body>

</html>
