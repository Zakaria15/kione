<?php include('include/connexion_bdd.php'); ?>


<!DOCTYPE html>
<html>

<?php include('include/head.php'); ?>

<body class="nopnom">
    <div class="container nopnom">
        <?php
        include('include/header.php');
        include('include/navigation.php');
        ?>

        <div class="nopnom premiere_image_accueil">
            <img src="images/car2.jpg" alt="" style="height : 100%; width : auto; min-width : 100%; opacity : 1;"/>
            <h1 class="texte_sur_premiere_image_accueil">
                Les investissements non cotées
                <br/>
                ouverts aux particuliers
                <br/>
                sans intermédiaire
            </h1>
        </div>

        <div style="display : flex; justify-content : space-around; padding : 2em 0;">
            <div class="titres_section_accueil" style="letter-spacing : 0.03em; color : #70ad47; text-align: center;">
                <span style="font-weight : 500;">Donnez du sens à votre épargne</span><br/>
                <span style="font-weight : 350;">Investissez dans l'économie réelle</span>
            </div>
        </div>

        <div class="section_donner_du_sens">

            <div class="section_donner_du_sens_enfant_1">
                <div class="nopnom petite_image_accueil">
                    <img src="images/sens1.png" alt="" style="height : auto; width : 100%;"/>
                    <div class="label_petite_image_accueil"> Innovation </div>
                </div>

                <div class="nopnom petite_image_accueil">
                    <img src="images/sens2.png" alt="" style="height : auto; width : 100%;"/>
                    <div class="label_petite_image_accueil"> Digitalisation </div>
                </div>

                <div class="nopnom petite_image_accueil">
                    <img src="images/sens3.png" alt="" style="height : auto; width : 100%;"/>
                    <div class="label_petite_image_accueil"> Croissance </div>
                </div>

                <div class="nopnom petite_image_accueil">
                    <img src="images/sens4.png" alt="" style="height : auto; width : 100%;"/>
                    <div class="label_petite_image_accueil"> Emploi </div>
                </div>
            </div>

            <div class="section_donner_du_sens_enfant_2">
                <div class="texte_section">
                    Soutenez les PME<br/>en finançant leur développement
                    <div style="position : absolute; top : 0; left : 12%; height : 8%; width : 33%; background-color : #002060;"></div>
                    <div style="position : absolute; bottom : 0; right : 0; height : 8%; width : 55%; background-color : #70ad47;"></div>
                    <div style="position : absolute; bottom : 0; left : 6%; height : 8%; width : 55%; background-color : #eb6c15; transform : translateY(300%);"></div>
                </div>
                <div class="boutton bouton_accueil bouton_accueil_1">
                    Découvrez l’univers des investissements non cotées ouverts aux particuliers
                </div>
            </div>
        </div>

        <div class="section_2" style="background-size: 100%; position : relative;">
            <div class="section_2_enfant_1" style="height : 100%; width : 100%;">
                <img src="images/financez_PME_locales.png" style="height : 100%; width : auto; position : absolute; top : 0; right : 0; z-index : -50;"/>
            </div>
            <div style="width : 100%; height: 10%; display : flex; justify-content : space-around; padding: 2em 0;">
                <div class="titres_section_accueil" style="letter-spacing : 0.03em; color : #70ad47; text-align: center;">
                    <span style="font-weight : 500;">Financez les PME locales</span><br/>
                    <span style="font-weight : 350;">Créez de l'emploi dans votre région</span>
                </div>
            </div>
            <div class="section_2_enfant_3">
                <div class="section_financer_PME_locales">
                    <div style="display : flex; flex-direction: column; justify-content: space-between; width : 30%;">
                        <div class="texte_section_2">
                                Les fonds d’investissement de <br/>
                                proximité vous permettent <br/>
                                d’investir localement et contribuer<br/>
                                ainsi à l’emploi près de chez vous
                                <div style="position : absolute; top : 0; left : 0; height : 5%; width : 33%; background-color : #002060;"></div>
                                <div style="position : absolute; bottom : 0; right : 10%; height : 5%; width : 45%; background-color : #70ad47;"></div>
                                <div style="position : absolute; bottom : 0; left : 15%; height : 5%; width : 45%; background-color : #eb6c15; transform : translateY(300%);"></div>
                        </div>
                        <div class="boutton bouton_accueil bouton_accueil_2">
                            Découvrez des exemples de sociétés financées
                        </div>
                    </div>
                    <div class="container_carte_France">
                        <img src="images/France.png" alt="" style="height : auto; width : 100%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div style="box-shadow: 0px 1px 5px lightgrey; display : flex; justify-content : space-around; background-color : #002060;">
            <div style="display : flex; flex-direction : column; justify-content: space-evenly; align-items: center;width : 100%; height : 100%;">
                <div class="titres_section_accueil" style="padding : 2em 0;">
                    <div style="text-align : center; color : #fff; font-weight : bold; margin-bottom : 5%;">
                        Investissez dans les sociétés de demain
                    </div>
                    <div style="text-align : center; color : #fff; font-weight : 100;">
                        Sélectionnées et portées par des experts de proximité
                    </div>
                </div>
                <div class="bouton_accueil bouton_accueil_3">
                    <a href="liste_fonds.php">
                        <div class="boutton">
                            Retrouvez l’ensemble des fonds ouverts actuellement
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="row ligne_vignettes" style="margin-top : 8em; margin-bottom : 3em;">
            <div class="ligne_vignettes_enfant">
                <div class="vignette vignette_accueil">
                    <div style="height: 55%;">
                        <img src="images/vign1.png  " alt="" style="height : 100%; width : 100%;"/>
                    </div>
                    <div style="height: 45%;">
                        <div style="text-align : center; margin-top : 1em; width: 80%; margin-left :auto;margin-right :auto; height : 65%;  border-bottom : 0.5px solid #70ad47">
                            Qu’est ce que la finance non cotée ?
                        </div>
                    </div>
                </div>
                <div class="vignette vignette_accueil">
                    <div style="height: 55%;">
                        <img src="images/vign2.jpg  " alt="" style="height : 100%; width : 100%;"/>
                    </div>
                    <div style="height: 45%;">
                        <div style="text-align : center; margin-top : 1em; width: 80%; margin-left :auto;margin-right :auto; height : 65%;  border-bottom : 0.5px solid #70ad47">
                            Pourquoi investir dans le non cotée ?
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row ligne_vignettes" style="margin-bottom : 3em;">
            <div class="ligne_vignettes_enfant">
                <div class="vignette vignette_accueil">
                    <div style="height: 55%;">
                        <img src="images/vign3.jpg  " alt="" style="height : 100%; width : 100%;"/>
                    </div>
                    <div style="height: 45%;">
                        <div style="text-align : center; margin-top : 1em; width: 80%; margin-left :auto;margin-right :auto; height : 65%;  border-bottom : 0.5px solid #70ad47">
                            Découvrir le fonctionnement d’un FCPR
                        </div>
                    </div>
                </div>
                <div class="vignette vignette_accueil">
                    <div style="height: 55%;">
                        <img src="images/vign4.jpg  " alt="" style="height : 100%; width : 100%;"/>
                    </div>
                    <div style="height: 45%;">
                        <div style="text-align : center; margin-top : 1em; width: 80%; margin-left :auto;margin-right :auto; height : 65%;  border-bottom : 0.5px solid #70ad47">
                            Quels sont les risques d’investir dans un FCPR ?
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include('include/articles.php'); ?>

        <?php include('include/footer.php'); ?>
    </div> <!-- container -->


    <?php include('include/javascript_menu.php'); ?>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>

    $( document ).ready(function() {
        $('.vignette').each(function(){
            $(this).hover(function(){
                $(this).css('box-shadow','0px 1px 10px black');
                $(this).css('cursor','pointer');
            });
            $(this).mouseleave(function(){
                $(this).css('box-shadow','0px 0px 0px black');

            });

        });

        $('.boutton').each(function(){
            $(this).hover(function(){
                $(this).css('background-color','#fff');
                $(this).css('color','#EB6C15');
                $(this).css('border-color','#eb6c15');
                $(this).css('cursor','pointer');

            });
            $(this).mouseleave(function(){
                $(this).css('background-color','#70ad47');
                $(this).css('color','#fff');
                $(this).css('border-color','#70ad47');
            });
        });

        $('.titre_actu').each(function(){
            $(this).hover(function(){
                $(this).css('color','#bb0b0b');
            });

            $(this).mouseleave(function(){
                $(this).css('color','black');
            });
        });
    })
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
