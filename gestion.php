<?php
session_start();
include('include/connexion_bdd.php');
$error_message = "";

function formate_date_to_lisible($date){
    $date_a_modif_split = explode('-', $date);
    $new_date = $date_a_modif_split[2] . '/' . $date_a_modif_split[1] . '/' . $date_a_modif_split[0];

    return $new_date;
}

function formate_date_to_stockable($date){
    $date_a_modif_split = explode('/', $date);
    $new_date = $date_a_modif_split[2] . '-' . $date_a_modif_split[1] . '-' . $date_a_modif_split[0];

    return $new_date;
}

if(isset($_POST['accessback'])){
    if($_POST['mdp'] == "gestionkioneadf&c_"){
        $_SESSION['admin'] = "true";
    }else{
        $error_message =  "Mot de passe incorrect";
    }
}

if(isset($_POST['leaveback'])){
        $_SESSION['admin'] = "false";
}

if(isset($_POST['maj_article'])){

    $date_a_stock = formate_date_to_stockable($_POST['date']);
    $afficher = 0;
    if(isset($_POST['afficher'])){
        $afficher = 1;
    }
    $req = $bdd->prepare('UPDATE articles SET titre = ?, date_article = ?, contenu  = ?, afficher = ? WHERE id = ?');
    $req->execute(array($_POST['titre'], $date_a_stock, $_POST['contenu'], $afficher, $_POST['id']));
}

if(isset($_POST['sup_article'])){

    $req = $bdd->prepare('DELETE FROM articles WHERE id = ?');
    $req->execute(array($_POST['id']));
}

if(isset($_POST['add_article'])){


    $uploaded_dir = $_SERVER['DOCUMENT_ROOT'] .'/images/articles/';
    $date_a_stock = formate_date_to_stockable($_POST['date']);
    // var_dump($_FILES['fileToUpload']);
    $uploadedfile = $uploaded_dir . basename($_FILES['fileToUpload']['name']);
    move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $uploadedfile);

    $req = $bdd->prepare('INSERT INTO articles(titre, image, date_article, contenu, afficher) VALUES(:titre, :image, :date_article, :contenu, :afficher)');
    $req->execute(array(
        'titre' => $_POST['titre'],
        'image' => $_FILES['fileToUpload']['name'],
        'date_article' => $date_a_stock,
        'contenu' => $_POST['contenu'],
        'afficher' => 1
    ));
}

?>


<!DOCTYPE html>
<html>

<?php include('include/head.php'); ?>

<body class="nopnom" style="background-color : rgba(0,180,0,0.4);">
    <div class="container nopnom">
        <?php if(isset($_SESSION['admin']) && $_SESSION['admin'] == "true"){ ?>
            <h1 style="margin-left : 10%; margin-top : 5vh;"> Gérer les articles </h1>

            <div style="display : flex; margin-left : 10%;">
                <div style="width : 20%; text-align : center;" class="border_test"> Titre </div>
                <div style="width : 10%; text-align : center;" class="border_test"> Date </div>
                <div style="width : 30%; text-align : center;" class="border_test"> Contenu </div>
                <div style="width : 5%; text-align : center;" class="border_test"> Afficher </div>
            </div>

            <?php $req = $bdd->query('SELECT * FROM articles');
            while($donnees = $req->fetch()){ ?>
                <form action="" method="POST" style="margin : 0;">
                <div style="display : flex; margin-left : 10%;">
                    <input type="hidden" name="id" value="<?= $donnees['id'] ?>">
                    <div style="width : 20%; display : flex; justify-content: center; align-items : center;" class="border_test">
                        <div>
                        <input type="text" name="titre" placeholder="Titre" style="width : 100%;" value="<?= $donnees['titre'] ?>" />
                        </div>
                     </div>
                    <div style="width : 10%; display : flex; justify-content: center; align-items : center;" class="border_test">
                        <input type="text" name="date" size="30" placeholder="Date" style="width : 80%;" value="<?= formate_date_to_lisible($donnees['date_article']) ?>" />
                     </div>
                    <div style="width : 30%;" class="border_test">
                        <textarea style="width : 100%;" name="contenu">
                            <?= $donnees['contenu'] ?>
                        </textarea>
                     </div>
                    <div style="width : 5%; display : flex; justify-content: center; align-items : center;" class="border_test">
                        <div>
                        <input type="checkbox" name="afficher" <?php if($donnees['afficher']){echo "checked";} ?> />
                    </div>
                     </div>
                 <div style="margin : auto 10px;">
                     <button type="submit" name="maj_article" class="btn btn-outline-secondary">
                         Mettre à jour
                     </button>
                 </div>
                 <div style="margin : auto 10px;">
                     <button type="submit" name="sup_article" class="btn btn-outline-danger">
                          Supprimer
                     </button>
                 </div>


                </div>
            </form>

            <?php
            }
            ?>


            <h1 style="margin-left : 10%; margin-top : 5vh;"> Ajouter un article </h1>

            <form enctype="multipart/form-data" action="" method="POST" style="margin-top : 20px; display : flex; flex-direction : column; margin-left : 10%;">
                <input type="text" name="titre" style="width : 50%; margin-bottom : 2em;" placeholder="Titre"> </input>
                <input type="text" name="date" style="width : 50%; margin-bottom : 2em;" placeholder="Date (jj/mm/aaaa)"> </input>

                <input type="hidden" name="MAX_FILE_SIZE" value="500000000" />
                Image <input type="file" size="10" name="fileToUpload" id="fileToUpload" accept=".png" style="margin-bottom : 2em;"/>

                <textarea name="contenu" cols="60" rows="5" placeholder="Contenu de l'article" value="" style="width : 50%; margin-bottom : 2em;">
                </textarea>

                <button type="submit" name="add_article" style="width : 50%; margin-bottom : 2em;"> Ajouter l'article </button>
            </form>

            <form action="" method="POST" style="position : fixed; top : 10px; right : 10px; width : 10%;">
                <button type="submit" name="leaveback" class="boutton" style="width : 100%;"> Se déconnecter </button>
            </form>

            <?php
        }else{ ?>
            <div style="height : 100vh; display : flex; justify-content: center; align-items : center;">
            <div style="height : 30%; width : 30%; background-color : #002060; border-width : 2px solid black; border-radius : 5px;">
            <form action="" method="POST" style="height : 100%; display : flex; flex-direction : column; justify-content : space-evenly; align-items : center;">
                <input type="text" name="mdp" style="width : 50%;"> </input>
                <label style="color : #eb6c15"> <?= $error_message ?> </label>
                <button class="boutton" type="submit" name="accessback" style="width : 30   %;"> Se connecter </button>
            </form>
            </div>
        </div>

            <?php
        }

         ?>
    </div> <!-- container -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

<script>

$(document).ready(function(){
    $('.boutton').each(function(){
        $(this).hover(function(){
            $(this).css('background-color','#fff');
            $(this).css('color','#eb6c15');
            $(this).css('border-color','#eb6c15');
            $(this).css('cursor','pointer');

        });
        $(this).mouseleave(function(){
            $(this).css('background-color','#70ad47');
            $(this).css('color','#fff');
            $(this).css('border-color','#70ad47');
        });
    });
})
</script>
</body>
</html>
