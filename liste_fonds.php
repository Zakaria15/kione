<?php include('include/connexion_bdd.php');
function formatdate($date){
    $date_exploded = explode("-",$date);

    return $date_exploded[2] . "/" . $date_exploded[1] . "/" . $date_exploded[0];
}?>

<!DOCTYPE html>
<html>

<?php include('include/head.php');
$now = date("Y-m-d",time());
?>

<body class="nopnom">
    <div class="container nopnom">
        <?php
        include('include/header.php');
        include('include/navigation.php');
        ?>
        <div class="row nopnom wrapper marge_haute titre_liste_de_fonds" style="text-align : center;">
            Les fonds ouverts aux souscritions en ce moment
        </div>


        <form id="form">
            <div class="row wrapper premier_filtre" style="border-bottom: solid 1px black">

                <div class="col-12 col-lg-2" style="text-align: center; white-space : nowrap; margin-bottom : 5px;">
                    Géographie :
                </div>
                <div class="col-12 col-lg-10 nopnom">
                    <div class="row">
                        <?php $req = $bdd->query('SELECT DISTINCT geographie FROM fonds WHERE fin_souscription>\'' . $now .'\' ORDER BY geographie ASC');
                        $tableau_geographie_1 = array();
                        $tableau_geographie_2 = array();
                        $tableau_geographie_final = array();

                        while ($donnees = $req->fetch())
                        {
                            $tableau_geographie_1[] = $donnees['geographie'];

                        }
                        foreach ($tableau_geographie_1 as &$line) {
                            $subline = explode("; ", $line);
                            foreach($subline as &$word){
                                $tableau_geographie_2[] = $word;
                            }
                        }
                        $tableau_geographie_final = array_unique($tableau_geographie_2);
                        sort($tableau_geographie_final);
                        $a = 0;
                        foreach ($tableau_geographie_final as &$value) {
                            if($value != "Inconnu" && $value != "Aucun particulier"){
                                $a += 1;
                            }
                        }
                        $a_count = 0;
                        foreach ($tableau_geographie_final as &$value) {
                            if($value != "Inconnu" && $value != "Aucun particulier"){
                                if($a % 4 == 0){
                                    if($a_count == 0 || $a_count == ($a/4)*1 || $a_count == ($a/4)*2 || $a_count == ($a/4)*3){
                                        echo "<div class=\"col-4 col-md-3 col-sm-4 col-xs-4\">";
                                    }

                                    ?>
                                    <div class="row">
                                        <div class="col-12 nopnom" style="white-space : nowrap; display : flex; align-items : center;">
                                            <input type="checkbox" name="<?= $value; ?>" class="checkbox_geo changer_liste_fonds"/>
                                            <?php echo $value?>
                                        </div>
                                    </div>

                                    <?php
                                    if($a_count == ($a/4)*1 - 1 || $a_count == ($a/4)*2 - 1 || $a_count == ($a/4)*3 - 1 || $a_count == $a - 1){
                                        echo "</div>";
                                    }
                                }else{
                                    if($a_count == 0 || $a_count == (intdiv($a,4) + 1)*1 || $a_count == (intdiv($a,4) + 1)*2 || $a_count == (intdiv($a,4) + 1)*3){
                                        echo "<div class=\"col-4 col-md-3 col-sm-4 col-xs-4\">";
                                    }

                                    ?>
                                    <div class="row">
                                        <div class="col-12 nopnom" style="white-space : nowrap; display : flex; align-items : center;">
                                            <input type="checkbox" name="<?= $value; ?>" class="checkbox_geo changer_liste_fonds"/>
                                            <?php echo $value?>
                                        </div>
                                    </div>

                                    <?php
                                    if($a_count == (intdiv($a,4) + 1)*1 - 1 || $a_count == (intdiv($a,4) + 1)*2 - 1 || $a_count == (intdiv($a,4) + 1)*3 - 1 || $a_count == $a - 1){
                                        echo "</div>";
                                    }
                                }
                                $a_count += 1;
                            }
                        } ?>
                    </div>
                </div>
            </div>

            <div class="row wrapper filtre" style="border-bottom: solid 1px black">
                <div class="col-12 col-lg-2" style="text-align: center; white-space : nowrap; margin-bottom : 5px;">
                    Secteur :
                </div>

                <div class="col-12 col-lg-10 nopnom">
                    <div class="row">
                        <?php $req = $bdd->query('SELECT DISTINCT secteur FROM fonds WHERE fin_souscription>\'' . $now .'\'');
                        $tableau_secteurs_1 = array();
                        $tableau_secteurs_2 = array();
                        $tableau_secteurs_final = array();
                        while ($donnees = $req->fetch())
                        {
                            $tableau_secteurs_1[] = $donnees['secteur'];
                        }
                        foreach ($tableau_secteurs_1 as &$line) {
                            $subline = explode("; ", $line);
                            foreach($subline as &$word){
                                $tableau_secteurs_2[] = $word;
                            }
                        }
                        $tableau_secteurs_final = array_unique($tableau_secteurs_2);
                        sort($tableau_secteurs_final);
                        $b = 0;
                        foreach ($tableau_secteurs_final as &$value) {
                            if($value != "Inconnu" && $value != "Aucun particulier"){
                                $b += 1;
                            }
                        }
                        $b_count = 0;
                        foreach ($tableau_secteurs_final as &$value) {
                            if($value != "Inconnu" && $value != "Aucun particulier"){
                                if($b % 4 == 0){
                                    if($b_count == 0 || $b_count == ($b/4)*1 || $b_count == ($b/4)*2 || $b_count == ($b/4)*3){
                                        echo "<div class=\"col-4 col-md-3 col-sm-4 col-xs-4\">";
                                    }

                                    ?>
                                    <div class="row">
                                        <div class="col-12 nopnom" style="white-space : nowrap; display : flex; align-items : center;">
                                            <input type="checkbox" name="<?= $value; ?>" class="checkbox_sec changer_liste_fonds"/>
                                            <?php echo $value?>
                                        </div>
                                    </div>

                                    <?php
                                    if($b_count == ($b/4)*1 - 1 || $b_count == ($b/4)*2 - 1 || $b_count == ($b/4)*3 - 1 || $b_count == $b - 1){
                                        echo "</div>";
                                    }
                                }else{
                                    if($b_count == 0 || $b_count == (intdiv($b,4) + 1)*1 || $b_count == (intdiv($b,4) + 1)*2 || $b_count == (intdiv($b,4) + 1)*3){
                                        echo "<div class=\"col-4 col-md-3 col-sm-4 col-xs-4\">";
                                    }

                                    ?>
                                    <div class="row">
                                        <div class="col-12 nopnom" style="white-space : nowrap; display : flex; align-items : center;">
                                            <input type="checkbox" name="<?= $value; ?>" class="checkbox_sec changer_liste_fonds"/>
                                            <?php echo $value?>
                                        </div>
                                    </div>

                                    <?php
                                    if($b_count == (intdiv($b,4) + 1)*1 - 1 || $b_count == (intdiv($b,4) + 1)*2 - 1 || $b_count == (intdiv($b,4) + 1)*3 - 1 || $b_count == $b - 1){
                                        echo "</div>";
                                    }
                                }
                                $b_count += 1;
                            }
                        } ?>
                    </div>
                </div>

            </div>
            <div class="row wrapper filtre" style="border-bottom: solid 1px black">

                <div class="col-12 col-lg-2" style="text-align: center; white-space : nowrap; margin-bottom : 5px;">
                    Gestionnaire :
                </div>
                <div class="col-12 col-lg-10 nopnom">
                    <div class="row">
                        <?php $req = $bdd->query('SELECT id, nom FROM gestionnaires ORDER BY nom');
                        $c = 0;
                        while ($donnees = $req->fetch())
                        {
                            $req2 = $bdd->prepare('SELECT id FROM fonds WHERE id_gestion = ? AND (fin_souscription>\'' . $now . '\' OR duree_min_de_placement <> 0)');
                            $req2->execute(array($donnees['id']));
                            if($req2->rowCount() > 0){
                                $c += 1;
                            }
                        }
                        $req = $bdd->query('SELECT id, nom FROM gestionnaires ORDER BY nom');
                        $c_count = 0;
                        while ($donnees = $req->fetch())
                        {
                            $req2 = $bdd->prepare('SELECT id FROM fonds WHERE id_gestion = ? AND (fin_souscription>\'' . $now . '\' OR duree_min_de_placement <> 0)');
                            $req2->execute(array($donnees['id']));
                            if($req2->rowCount() > 0){
                                if($c % 4 == 0){
                                    if($c_count == 0 || $c_count == ($c/4)*1 || $c_count == ($c/4)*2 || $c_count == ($c/4)*3){
                                        echo "<div class=\"col-6 col-md-3 col-sm-4 col-xs-4\">";
                                    }
                                    ?>
                                    <div class="row">
                                        <div class="col-12 nopnom" style="white-space : nowrap; display : flex; align-items : center;">
                                            <input type="checkbox" name="<?= $donnees['nom']; ?>" class="checkbox_gestion changer_liste_fonds"/>
                                            <?php echo $donnees['nom']; ?>
                                        </div>
                                    </div>
                                    <?php
                                    if($c_count == ($c/4)*1 - 1 || $c_count == ($c/4)*2 - 1 || $c_count == ($c/4)*3 - 1 || $c_count == $c - 1){
                                        echo "</div>";
                                    }
                                }else{
                                    $test = 1;
                                    if($c_count == 0 || $c_count == (intdiv($c,4) + $test)*1 || $c_count == (intdiv($c,4) + $test)*2 || $c_count == (intdiv($c,4) + $test)*3){
                                        echo "<div class=\"col-6 col-md-3 col-sm-4 col-xs-4\">";
                                    }
                                    ?>
                                    <div class="row">
                                        <div class="col-12 nopnom" style="white-space : nowrap; display : flex; align-items : center;">
                                            <input type="checkbox" name="<?= $donnees['nom']; ?>" class="checkbox_gestion changer_liste_fonds"/>
                                            <?php echo $donnees['nom']; ?>
                                        </div>
                                    </div>
                                    <?php
                                    if($c_count == (intdiv($c,4) + $test)*1 - 1 || $c_count == (intdiv($c,4) + $test)*2 - 1 || $c_count == (intdiv($c,4) + $test)*3 - 1 || $c_count == $c - 1){
                                        echo "</div>";
                                    }
                                }
                                $c_count += 1;
                            }
                        } ?>
                    </div>
                </div>
            </div>
            <div class="row wrapper filtre" style="border-bottom: solid 1px black">
                <div class="col-12 col-lg-2" style="text-align: center; white-space : nowrap; margin-bottom : 5px;">
                    Statut :
                </div>
                <div class="col-12 col-lg-10" style="white-space : nowrap;">
                    <div class="row">
                        <div class="col-6" style="display : flex; align-items : center;">
                            <input type="checkbox" name="Ouvert actuellement" class="checkbox_stat changer_liste_fonds"/>
                            Ouvert actuellement
                        </div>
                        <div class="col-3" style="display : flex; align-items : center;">
                            <input type="checkbox" name="Toujours ouvert" class="checkbox_stat changer_liste_fonds"/>
                            Toujours ouvert
                        </div>
                    </div>
                </div>

            </div>

            <div class="row wrapper filtre reset_filtres_container">
                <div class="col-6 col-lg-3" style="white-space : nowrap;">
                    Montant de souscription minimum :
                </div>
                <div class="col-6 col-lg-3" style="display : flex; align-items : center;">
                    <?php $req = $bdd->query('SELECT MIN(souscription_min) as sous_min FROM fonds WHERE fin_souscription>\'' . $now .'\'');
                    $donnees = $req->fetch();
                    $sous_min = $donnees['sous_min'];
                    ?>
                    <?php $req = $bdd->query('SELECT MAX(souscription_min) as sous_max FROM fonds WHERE fin_souscription>\'' . $now .'\'');
                    $donnees = $req->fetch();
                    $sous_max = $donnees['sous_max'];
                    ?>
                    <output><?= number_format($sous_min,0,'',' '); ?></output>
                    <span> €</span>
                    <input type='range' id='min_montant' class="changer_liste_fonds" name='sous_min' min='<?= $sous_min ?>' max='<?= $sous_max ?>' value='<?= $sous_max ?>'​/>
                    <output id='label_montant_1'><?= number_format($sous_max,0,'',' '); ?></output>
                    <span> €</span>
                </div>
                <div class="col-6 col-lg-3">
                    Souscription en ligne :
                </div>
                <div class="col-6 col-lg-3" style="white-space : nowrap;">
                    <input type="checkbox" id="sous_ligne" class="changer_liste_fonds"/>
                </div>
                <button type="button" class="btn btn-dark" id="reset_filtres">Réinitialiser filtres</button>
            </div>
        </form>

    </div>
    <!--
    <div class="row wrapper filtre">
    <div class="col-12">



    <div class="row">
    <div class="col-2">
    Société de gestion :
</div>
<div class="col-4">
<div class="row">

<?php /*$req = $bdd->query('SELECT DISTINCT gestion FROM fonds ORDER BY gestion ASC');
while ($donnees = $req->fetch())
{ ?>
<div class="col-6">
<input type="checkbox" name="<?= $donnees['gestion']; ?>" id="checbox" name=""/>
<?php echo $donnees['gestion']; ?>

</div>
<?php } ?>
</div>
</div>

<div class="col-2" style="border-left: solid 1px black">
Secteur :
</div>
<div class="col-4">
<div class="row">

<?php $req = $bdd->query("SELECT DISTINCT secteur FROM fonds");
$tableau_secteurs_1 = array();
$tableau_secteurs_2 = array();
$tableau_secteurs_final = array();
while ($donnees = $req->fetch())
{
$tableau_secteurs_1[] = $donnees['secteur'];
}
foreach ($tableau_secteurs_1 as &$line) {
$subline = explode("; ", $line);
foreach($subline as &$word){
$tableau_secteurs_2[] = $word;
}
}
$tableau_secteurs_final = array_unique($tableau_secteurs_2);
sort($tableau_secteurs_final);
foreach ($tableau_secteurs_final as &$value) { ?>
<div class="col-6">
<input type="checkbox" name="<?= $value; ?>" id="checbox" name=""/>
<?php echo $value?>
<br/>
</div>
<?php } ?>



</div>
</div>
</div>
<div class="row" style="border-top: solid 1px black">
<div class="col-2">
Géographie :
</div>
<div class="col-4">
<div class="row">

<?php $req = $bdd->query('SELECT DISTINCT geographie FROM fonds ORDER BY geographie ASC');
while ($donnees = $req->fetch())
{ ?>
<div class="col-6">
<input type="checkbox" name="<?= $donnees['geographie']; ?>" id="checbox" name=""/>
<?php echo $donnees['geographie']; ?>
</div>
<?php } ?>
</div>
</div>
<div class="col-2" style="border-left: solid 1px black">
Statut :
</div>
<div class="col-4">
<div class="row">

<?php $req = $bdd->query('SELECT DISTINCT statut FROM fonds ORDER BY statut ASC');
while ($donnees = $req->fetch())
{ ?>
<div class="col-6">
<input type="checkbox" name="<?= $donnees['statut']; ?>" id="checbox" name=""/>
<?php echo $donnees['statut']; ?>
</div>
<?php } */?>
</div>
</div>
</div>
</div>
-->

</div>
<div id="txtHint">
    <?php
    $req = $bdd->query('SELECT * FROM fonds WHERE fin_souscription>\'' . $now .'\'');
    while ($donnees = $req->fetch())
    {
        $req2 = $bdd->prepare('SELECT * FROM gestionnaires WHERE id = ?');
        $req2->execute(array($donnees['id_gestion']));
        $donnees2 = $req2->fetch();
        echo "<div class=\"row wrapper article\">";
        echo "<div class=\"col-4 col-lg-2 nopnom\" style=\"width : 100%; height : 100%\" >";
        echo "<img style=\"max-width : 100%; max-height : 100%; width : auto; height : auto; display: block; position : absolute; top : 50%; left : 50%; transform : translate(-50%, -50%);\" src=\"images/logo_gestion/" . $donnees2['img'] . "\" alt\"\"/>";
        echo "</div>";
        echo "<div class=\"col-8 col-lg-10\" style=\"padding-left : 0; padding-right : 0;\">";
        echo "<div class=\"row centrer premiere_ligne_fond\">";
        echo "<div class=\"col-12 col-lg-7\" style=\"line-height : 1.5em;\">";
        echo "<span class=\"nom_article\">" . $donnees['nom']. "</span>";
        echo "<br/>";
        echo "<span class=\"gestion_article\">Géré par " . $donnees2['nom'] . "</span>";
        echo "</div>";

        echo "<a href=\"" . $donnees['decouvrir'] ."\" target=\"_blank\">";
        echo "<div class=\"col-2\" style=\"margin-left : 15px; padding-left : 0px; padding-right : 0px;\">";
        echo "<div class=\"bouton_article_dec\"> DECOUVRIR </div>";
        echo "</a>";
        echo "</div>";
        if($donnees['souscription_en_ligne']){
            echo "<div class=\"col-1\">";
            echo "</div>";
            echo "<div class=\"col-2\" style=\"margin-left : 15px; padding-left : 0px; padding-right : 0px;\">";
            echo "<a href=\"" . $donnees['souscrire'] ."\" target=\"_blank\">";
            echo "<div class=\"bouton_article_sous\"> SOUSCRIRE </div>";
            echo "</a>";
            echo "</div>";
        }
        else{
            echo "<div class=\"col-1\">";
            echo "</div>";
            echo "<div class=\"col-2\">";
            echo "</div>";
        }
        echo "</div>";
        echo "<div class=\"row centrer deuxieme_ligne_fond\">";
        echo "<div class=\"col-12 col-lg-3\">";
        if($donnees['duree_min_de_placement'] > 0){
            $statut = "Toujours ouvert";
        }else{
            $statut = "Ouvert actuellement";
        }
        echo "<span class=\"gras\">Statut : </span>" . $statut;
        echo "<br/>";
        echo "<span class=\"gras\">Souscription minimum : </span>" . number_format($donnees['souscription_min'], 0, '', ' ') . " €";
        echo "<br/>";
        echo "<span class=\"blank_line\">&nbsp;</span>";
        echo "</div>";
        echo "<div class=\"col-12 col-lg-3\">";
        echo "<span class=\"gras\">Date de création : </span>";
        if($donnees['date_creation'] != "1900-01-01"){
            echo formatdate($donnees['date_creation']);
        }
        echo "<br/>";
        if($donnees['duree_min_de_placement'] > 0){
            echo "<span class=\"gras\">Durée min de placement : </span>";
            if($donnees['duree_min_de_placement'] != 100){
                echo $donnees['duree_min_de_placement'] . " ans";
            }
            echo "<br/>";
            echo "<span class=\"gras\" style=\"visibility : hidden\">F</span>";
        }else{
            echo "<span class=\"gras\">Fin de souscription : </span>";
            if($donnees['fin_souscription'] != "2100-01-01"){
                echo formatdate($donnees['fin_souscription']);
            }
            echo "<br/>";
            echo "<span class=\"gras\">Date de déblocage : </span>";
            if($donnees['date_deblocage'] != "1900-01-01"){
                echo formatdate($donnees['date_deblocage']);
            }
            echo "<br/>";
            echo "<span class=\"blank_line\">&nbsp;</span>";
        }
        echo "</div>";
        echo "<div class=\"col-12 col-lg-6\">";
        echo "<span class=\"gras\"> Secteur : </span>";
        if($donnees['secteur'] != "Inconnu"){
            echo $donnees['secteur'];
        }
        echo "<br/>";
        echo "<span class=\"gras\"> Géographie : </span>";
        if($donnees['geographie'] != "Inconnu"){
            echo $donnees['geographie'];
        }
        echo "<br/>";
        echo "<span class=\"gras\"> Profil de risque et de rendement : </span>" . $donnees['profil_risque'];
        echo "</div>";
        echo "</div>";
        echo "</div>";
        echo "</div>";

    }
    $req->closeCursor();
    ?>

</div>
<div class="wrapper">
    Société de gestion, vous n'êtes pas encore référencé chez nous ou une information n'est plus à jour <a href="mailto:contact@investissement-utile.fr" style="color : green;"> contactez nous. </a>

</div>

<?php include('include/footer.php'); ?>
</div> <!-- container -->
<?php include('include/javascript_menu.php'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script>
$( document ).ready(function() {

    $('#min_montant').on('input',function () {
        prix_format = Intl.NumberFormat().format(document.getElementById('min_montant').value);
        $('#label_montant_1').text(prix_format);
    });

    function findCheckedItems(check_class) {

        var checkedItems = [];

        var form = document.getElementById('form');

        var inputs = form.getElementsByClassName(check_class);
        var checkboxIndex = 0;

        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].type.toLowerCase() === 'checkbox') {
                if (inputs[i].checked) {
                    var data = inputs[i].name;
                    checkedItems.push('"'+data+'"');
                    // do whatever you like with the data
                }
                checkboxIndex++;
            }
        }

        return checkedItems;
    }

    $('.changer_liste_fonds').change(function(){
        var criteres_geo = [];
        var criteres_sec = [];
        var criteres_stat = [];
        criteres_geo = findCheckedItems("checkbox_geo");
        criteres_sec = findCheckedItems("checkbox_sec");
        criteres_stat = findCheckedItems("checkbox_stat");
        criteres_gestion = findCheckedItems("checkbox_gestion");
        prix_minimum = document.getElementById('min_montant').value;
        sous_ligne = $('#sous_ligne').is(':checked');
        var xhttp;
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
            }
        };
        xhttp.open("GET", "getfonds.php?geo="+criteres_geo+"&sec="+criteres_sec+"&stat="+criteres_stat+"&gestion="+criteres_gestion+"&prix="+prix_minimum+"&sous_ligne="+sous_ligne, true);
        xhttp.send();
    });

    $('#reset_filtres').click(function(){
        $(':checkbox').prop( "checked", false );
        var criteres_geo = [];
        var criteres_sec = [];
        var criteres_stat = [];
        criteres_geo = findCheckedItems("checkbox_geo");
        criteres_sec = findCheckedItems("checkbox_sec");
        criteres_stat = findCheckedItems("checkbox_stat");
        prix_minimum = document.getElementById('min_montant').max;
        document.getElementById('min_montant').value = document.getElementById('min_montant').max;
        document.getElementById('min_montant').nextElementSibling.value = document.getElementById('min_montant').value;
        var xhttp;
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
            }
        };
        xhttp.open("GET", "getfonds.php?geo="+criteres_geo+"&sec="+criteres_sec+"&stat="+criteres_stat+"&prix="+prix_minimum, true);
        xhttp.send();
    });

});
</script>

</body>
</html>
